#include "params.h"

// General constants
#define PI 3.141593f		// pi number

__constant int OUTPUT_SIZE = 256 * 256;
__constant float INPUT_STEP  = 0.005;	// _x, _y step in mm
__constant float OUTPUT_STEP = 0.00005;	// z, y   step in mm
__constant int Z_OFFSET = 62000;	// offset to the focus

__constant float K_0 = 2 * M_PI / LAMBDA * INTEGRATION_STEP;	// dimentionless wavenumber
__constant float F_0 = FOCAL_LENGTH / INTEGRATION_STEP;		// dimentionless focus

// Abberation constants
__constant float abber_f_0 = 5.0f / 0.00003f;	// normalized abberation focus
__constant float ref_index = 1.515;	// refraction index

__kernel void getIntensity(__global float * real_output, 	// real part of the integral
						   __global float * imag_output, 	// imaginary part of the integral
						   __global float * result)      	// resulting intensity
{
	int n = get_global_id(0);	// z output coordinate, pixels
	int m = get_global_id(1);	// y output coordinate, pixels

	float z = n + Z_OFFSET - (OUTPUT_WIDTH - 1) / 2.0f;		// normalized z output coordinate

	float Im = imag_output[n + m * OUTPUT_WIDTH];
	float Re = real_output[n + m * OUTPUT_WIDTH];

	result[n + m * OUTPUT_WIDTH] = (Re * Re + Im * Im) / z / z;
}

__kernel void compute(__global const float *ampl,
					  __global const float *real_phase,
					  __global const float *imag_phase,
					  __global float *ampl_output,
					  __global float *real_phase_output,
					  __global float *imag_phase_output)
{
	/*
	 * Creating local arrays to load global values
	 */
	__local float ampl_l[WORKGROUP_SIZE];
	__local float real_phase_l[WORKGROUP_SIZE * PHI_SIZE];
	__local float imag_phase_l[WORKGROUP_SIZE * PHI_SIZE];

	/*
	 * Load globals
	 * amplitude is to be wholy loaded into local buffer
	 */
	for(int i = RADIAL_SIZE / WORKGROUP_SIZE - 1; i >= 0; i--)
		ampl_l[get_local_id(0) + i * WORKGROUP_SIZE] = ampl[get_local_id(0) + i * WORKGROUP_SIZE];
	for(int i = 0; i < PHI_SIZE; i++)
	{
		real_phase_l[get_local_id(0)] = real_phase[get_global_id(0)];
	}


}

__kernel void fill_integral_buff(__global const float *real_input,
								 __global const float *imag_input,
								 __global float *re_buff,
								 __global float *im_buff)
{
	/*
	 * NDRange is supposed to be 2D
	 *
	 *		 WORKGROUP_SIZE
	 *  	  <---phi--->
	 *		 ^ x x x x x
	 *		 | x x x x x
	 *		 r x x x x x
	 *		 | x x x x x
	 *	 	 v
	 */	
	local float real[WORKGROUP_SIZE];
	local float imag[WORKGROUP_SIZE];

	// Load
	real[get_global_id(0)] = real_input[get_local_id(0) + get_group_id(1) * WORKGROUP_SIZE];
	imag[get_global_id(0)] = imag_input[get_local_id(0) + get_group_id(1) * WORKGROUP_SIZE];


}

__kernel void clear_integral_buff(global float *re_buff,
								  global float *im_buff)
{
	// Takes 1D array
	re_buff[get_global_id(0)] = im_buff[get_global_id(0)] = 0;
}

__kernel void clear_output_buff(global float *re_buff,
								  global float *im_buff)
{
	// Takes 1D array
	re_buff[get_global_id(0)] = im_buff[get_global_id(0)] = 0;
}

/* test kernel */
__kernel void __echo(__global float *input_buffer,
					 __global float *output_buffer
					)
{
	int gid = get_global_id(0);

	output_buffer[gid] = input_buffer[gid];
}
