// TODO: add conditional include
// Macros for simulate_beam
#define WORKGROUP_SIZE 256

#define INTEGRATION_STEP 0.00005f			// the step in mm
#define XY_STEP	0.005f						// the step in mm
#define RADIAL_STEP 0.003f                  // step in mm
#define PHI_STEP (2.f * PI / (float)PHI_SIZE)   // step in radians

#define WG_SIZE 256
#define NDRANGE_SIZE 4096
#define WORK_SIZE 256					    // WG size
#define PHI_SIZE 256                        // num of points along phi axis of input image
#define RADIAL_SIZE 1024                    // num of points along rho axis of input image
#define Z_SIZE  256                         // num of points along z axis of output image
// TODO: remove INPUT/OUTPUT_WIDTH/HEIGHT
#define INPUT_WIDTH  (WORK_SIZE * 4)		// corresponds to x, in pixels
#define INPUT_HEIGHT (WORK_SIZE * 4)		// corresponds to y, in pixels
#define OUTPUT_WIDTH    256
#define OUTPUT_HEIGHT   256
#define WORKGROUP_NUM OUTPUT_HEIGHT * OUTPUT_WIDTH / WORKGROUP_SIZE // size of NDRange
#define WORK_SIZE OUTPUT_HEIGHT * OUTPUT_WIDTH  // size of NDRange

#define BEAM_RADIUS 0.693f	           		// beam radius, mm
#define LAMBDA 0.000770f                       // wavelength, mm
#define FOCAL_LENGTH 3.1f                   // focal length, mm
