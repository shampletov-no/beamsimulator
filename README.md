# beamSimulator

Project simulates beam propagation through a lens with controllable aberrations. As a result of running `beamSimulator.exe` you will get a longitudinal plane image `output.jpeg` of focused beam.

## Installation

<u>First variant</u>. Here I describe an example of installation, so you can deviate from following sequence of actions. You require:

* Windows 10 (maybe other versions also will work, I didn't check)
* GPU that has OpenCL1.2 or higher implementation
* Visual Studio (in theory you can also use Xcode or Eclipse CDT)
* CMake 3.10 or higher

The sequence:

1. Clone the repo or download and extract archive to any folder you want. For example to folder `${YOUR_PATH}\beamSimulator`
2. Go to that folder
3. Make directory `build` in `beamSimulator` folder. Go to `build`.
4. Run `cmd` in that directory and type `cmake ..` to create project.
5. Then type `cmake --build . --config Release` to build the project.
6. Finally use `cmake --install .` command for installing an executable and necessary satellite files in `build\bin` directory.

<u>Second variant</u>. If you are pretty lucky and have Windows x64, you can try run installer `Beam Simulator-x.x.x-win64.exe`.

#### DEBUG mode

To use debug mode type `cmake .. -DDEBUG=ON`  at step 4.

## Usage

Consider you go through the installation sequence above. In that case simply run `${YOUR_PATH}\beamSimulator\build\bin\beamSimulator.exe`. After some time (maybe several minutes) computed image will be located to `bin\images`.
