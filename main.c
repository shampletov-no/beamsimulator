#define _CRT_SECURE_NO_WARNINGS
#include "CL\cl.h"
#include "FreeImage.h"
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include <complex.h>
#include "params.h"

// include configuration file
#include "beamSimulatorConfig.h"

#define PLATFORM_INDEX 0	// index of platform to be used
#define DEVICE_INDEX 0		// index of GPU device to be used

#define PI 3.141593f		// pi number

// Normalized values
const float k_0 = 2 * PI / LAMBDA * INTEGRATION_STEP;		// normalized wave number,  dimensionless
const float f_0 = FOCAL_LENGTH / INTEGRATION_STEP;		// normalized focal length, dimensionless

/* errCheck: checks for error and in bad case printing error massage and return EXIT_FAILURE */
short errCheck(cl_int errNum, const char* error);

float *GetRealArray(float complex *buff);
float *GetImagArray(float complex *buff);
cl_context createContext(void);
cl_program createProgram(cl_context context);
cl_command_queue createCommandQueue(cl_context context, cl_program program);

// save mode list
typedef enum {
	INPUT_MODE,
	OUTPUT_MODE,
	INTENSITY_MODE,
	AMPLITUDE_MODE,
	PHASE_MODE,
	POLAR_MODE
} save_mode_t;

typedef struct
{
	cl_mem Re;
	cl_mem Im;
} cl_complex_mem;

void freeComplexBuff(cl_complex_mem *buff)
{
	clReleaseMemObject(buff->Re);
	clReleaseMemObject(buff->Im);
}


// TODO: remove fillInput, __saveInputImage
void fillInput(float *real_input, float *imag_input);
void fillPolarInput(float complex *input_field);
int saveImage(float *buffer, char *filename, float pix_size, save_mode_t mode);
void __saveInputImage(float*, char*, save_mode_t);
float* normalize(float* buffer, int size);
void getColour(float grey_value, BYTE* output_colour);

int main(int argc, const char** argv)
{
	cl_int errNum;
	cl_context context;
	cl_program program;
	cl_command_queue commandQueue; // queues for first and second GPUs
	float complex *input;
	float complex *integral_buff;
	float *result, *__test;
	cl_mem cl_result;
	cl_complex_mem cl_output, cl_input, cl_integral_buff;

	/* printing version info */
	printf("beamSimulator \nversion: %d.%d.%d\n",
				beamSimulator_VERSION_MAJOR,
				beamSimulator_VERSION_MINOR,
				beamSimulator_VERSION_PATCH
				);
#ifdef DEBUG
	printf("DEBUG mode: ON\n\n");
#endif

	// Create context
	context = createContext();
	program = createProgram(context);
	commandQueue = createCommandQueue(context, program);


	//***************                       ***************//
	//**********     Creating memory objects     **********//
	//**************                        ***************//
	input  = (complex float*)malloc(RADIAL_SIZE * PHI_SIZE * sizeof(float complex));
	__test  = (float*)malloc(RADIAL_SIZE * PHI_SIZE * sizeof(float));
	result = (float*)malloc(OUTPUT_HEIGHT * OUTPUT_WIDTH * sizeof(float));
	
	fillPolarInput(input);

	// Create input objects
	cl_input.Re = clCreateBuffer(context,
		CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
		sizeof(cl_float) * RADIAL_SIZE * PHI_SIZE,
		GetRealArray(input),
		&errNum);
	if (errCheck(errNum, "cl_input = clCreateBuffer(...)") == EXIT_FAILURE)
	{
		free(result);
		free(input);
		free(__test);
		freeComplexBuff(&cl_input);
		clReleaseCommandQueue(commandQueue);
		clReleaseProgram(program);
		clReleaseContext(context);
		return EXIT_FAILURE;
	}
	cl_input.Im = clCreateBuffer(context,
		CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
		sizeof(cl_float) * RADIAL_SIZE * PHI_SIZE,
		GetImagArray(input),
		&errNum);
	if (errCheck(errNum, "cl_input = clCreateBuffer(...)") == EXIT_FAILURE)
	{
		free(result);
		free(input);
		free(__test);
		freeComplexBuff(&cl_input);
		clReleaseCommandQueue(commandQueue);
		clReleaseProgram(program);
		clReleaseContext(context);
		return EXIT_FAILURE;
	}
	
	// Create integral buffer objects
	cl_integral_buff.Re = clCreateBuffer(context,
		CL_MEM_READ_WRITE,
		sizeof(float) * RADIAL_SIZE * PHI_SIZE,
		NULL,
		&errNum);
	if (errCheck(errNum, "cl_integral_buff = clCreateBuffer(...)") == EXIT_FAILURE)
	{
		free(result);
		free(input);
		free(__test);
		freeComplexBuff(&cl_input);
		freeComplexBuff(&cl_integral_buff);
		clReleaseMemObject(cl_result);
		clReleaseCommandQueue(commandQueue);
		clReleaseProgram(program);
		clReleaseContext(context);
		return EXIT_FAILURE;
	}
	cl_integral_buff.Im = clCreateBuffer(context,
		CL_MEM_READ_WRITE,
		sizeof(float) * RADIAL_SIZE * WORKGROUP_SIZE,
		NULL,
		&errNum);
	if (errCheck(errNum, "cl_integral_buff = clCreateBuffer(...)") == EXIT_FAILURE)
	{
		free(result);
		free(input);
		free(__test);
		freeComplexBuff(&cl_input);
		freeComplexBuff(&cl_integral_buff);
		clReleaseMemObject(cl_result);
		clReleaseCommandQueue(commandQueue);
		clReleaseProgram(program);
		clReleaseContext(context);
		return EXIT_FAILURE;
	}

	// Create output objects
	cl_output.Re = clCreateBuffer(context,
		CL_MEM_READ_WRITE,
		sizeof(cl_float) * RADIAL_SIZE * PHI_SIZE,
		NULL,
		&errNum);
	if (errCheck(errNum, "cl_output = clCreateBuffer(...)") == EXIT_FAILURE)
	{
		free(result);
		free(input);
		free(__test);
		freeComplexBuff(&cl_input);
		freeComplexBuff(&cl_output);
		clReleaseCommandQueue(commandQueue);
		clReleaseProgram(program);
		clReleaseContext(context);
		return EXIT_FAILURE;
	}
	cl_output.Im = clCreateBuffer(context,
		CL_MEM_READ_WRITE,
		sizeof(cl_float) * RADIAL_SIZE * PHI_SIZE,
		NULL,
		&errNum);
	if (errCheck(errNum, "cl_output = clCreateBuffer(...)") == EXIT_FAILURE)
	{
		free(result);
		free(input);
		free(__test);
		freeComplexBuff(&cl_input);
		freeComplexBuff(&cl_output);
		clReleaseCommandQueue(commandQueue);
		clReleaseProgram(program);
		clReleaseContext(context);
		return EXIT_FAILURE;
	}

	// Create object for intensity
	cl_result = clCreateBuffer(context,
		CL_MEM_WRITE_ONLY,
		sizeof(cl_float) * OUTPUT_HEIGHT * OUTPUT_WIDTH,
		NULL,
		&errNum);
	if (errCheck(errNum, "cl_result = clCreateBuffer(...)") == EXIT_FAILURE)
	{
		free(result);
		free(input);
		free(__test);
		freeComplexBuff(&cl_input);
		freeComplexBuff(&cl_output);
		clReleaseMemObject(cl_result);
		clReleaseCommandQueue(commandQueue);
		clReleaseProgram(program);
		clReleaseContext(context);
		return EXIT_FAILURE;
	}
	// Release buffers on host, they were necessary for copying their values to GPU.
	free(input);

	//**************                ***************//
	//*********     Creating kernels     **********//
	//**************                ***************//
	// 0. Creating "__echo" kernel, use for tests
	cl_kernel __echo_kernel = clCreateKernel(program, "__echo", &errNum);
	if (errCheck(errNum, "clCreateKernel(\"hetIntensity\""))
	{
		free(result);
		free(__test);
		clReleaseKernel(__echo_kernel);
		freeComplexBuff(&cl_input);
		freeComplexBuff(&cl_output);
		clReleaseMemObject(cl_result);
		freeComplexBuff(&cl_integral_buff);
		clReleaseCommandQueue(commandQueue);
		clReleaseProgram(program);
		clReleaseContext(context);
		return EXIT_FAILURE;
	}

	// 1. Creating "getIntensity" kernel
	cl_kernel getIntensity_kernel = clCreateKernel(program, "getIntensity", &errNum);
	if (errCheck(errNum, "clCreateKernel(\"hetIntensity\""))
	{
		free(result);
		free(__test);
		clReleaseKernel(__echo_kernel);
		clReleaseKernel(getIntensity_kernel);
		freeComplexBuff(&cl_input);
		freeComplexBuff(&cl_output);
		clReleaseMemObject(cl_result);
		clReleaseCommandQueue(commandQueue);
		clReleaseProgram(program);
		clReleaseContext(context);
		return EXIT_FAILURE;
	}

	// 2. Create "compute" kernel
	cl_kernel compute_kernel = clCreateKernel(program, "compute", &errNum);
	if (errCheck(errNum, "clCreateKernel(\"integrate\""))
	{
		free(result);
		free(__test);
		clReleaseKernel(__echo_kernel);
		clReleaseKernel(getIntensity_kernel);
		clReleaseKernel(compute_kernel);
		freeComplexBuff(&cl_input);
		freeComplexBuff(&cl_output);
		clReleaseMemObject(cl_result);
		clReleaseCommandQueue(commandQueue);
		clReleaseProgram(program);
		clReleaseContext(context);
		return EXIT_FAILURE;
	}

	// 3. Create "clear_integral_buff" kernel
	cl_kernel clear_integral_buff_kernel = clCreateKernel(program, "clear_integral_buff", &errNum);
	if (errCheck(errNum, "clCreateKernel(\"clear_integral_buff\""))
	{
		free(result);
		free(__test);
		clReleaseKernel(__echo_kernel);
		clReleaseKernel(getIntensity_kernel);
		clReleaseKernel(compute_kernel);
		clReleaseKernel(clear_integral_buff_kernel);
		freeComplexBuff(&cl_input);
		freeComplexBuff(&cl_output);
		clReleaseMemObject(cl_result);
		clReleaseCommandQueue(commandQueue);
		clReleaseProgram(program);
		clReleaseContext(context);
		return EXIT_FAILURE;
	}

	// 4. Create "clear_output_buff" kernel
	cl_kernel clear_output_buff_kernel = clCreateKernel(program, "clear_output_buff", &errNum);
	if (errCheck(errNum, "clCreateKernel(\"clear_integral_buff\""))
	{
		free(result);
		free(__test);
		clReleaseKernel(__echo_kernel);
		clReleaseKernel(getIntensity_kernel);
		clReleaseKernel(compute_kernel);
		clReleaseKernel(clear_integral_buff_kernel);
		clReleaseKernel(clear_output_buff_kernel);
		freeComplexBuff(&cl_input);
		freeComplexBuff(&cl_output);
		clReleaseMemObject(cl_result);
		clReleaseCommandQueue(commandQueue);
		clReleaseProgram(program);
		clReleaseContext(context);
		return EXIT_FAILURE;
	}

	//**************                         ***************//
	//*********     Setting kernels arguments     **********//
	//**************                         ***************//
#ifdef USE_ECHO
	// 0. __echo kernel
	errNum |= clSetKernelArg(__echo_kernel, 0, sizeof(cl_mem), &cl_input_ampl);
	errNum |= clSetKernelArg(__echo_kernel, 1, sizeof(cl_mem), &cl_result);
#endif

	// 1. getIntensity kernel
	errNum |= clSetKernelArg(getIntensity_kernel, 0, sizeof(cl_mem), &cl_output.Re);
	errNum |= clSetKernelArg(getIntensity_kernel, 1, sizeof(cl_mem), &cl_output.Im);
	errNum |= clSetKernelArg(getIntensity_kernel, 2, sizeof(cl_mem), &cl_result);

	// 2. compute kernel
	errNum |= clSetKernelArg(compute_kernel, 0, sizeof(cl_mem), &cl_input.Re);
	errNum |= clSetKernelArg(compute_kernel, 1, sizeof(cl_mem), &cl_input.Im);
	errNum |= clSetKernelArg(compute_kernel, 2, sizeof(cl_mem), &cl_output.Re);
	errNum |= clSetKernelArg(compute_kernel, 3, sizeof(cl_mem), &cl_output.Im);

	// 3. clear_buff kernel
	errNum |= clSetKernelArg(clear_integral_buff_kernel, 0, sizeof(cl_mem), &cl_integral_buff.Re);
	errNum |= clSetKernelArg(clear_integral_buff_kernel, 1, sizeof(cl_mem), &cl_integral_buff.Im);

	if (errCheck(errNum, "clSetKernelArg(...)") == EXIT_FAILURE)
	{
		free(result);
		free(__test);
		clReleaseKernel(getIntensity_kernel);
		clReleaseKernel(compute_kernel);
		clReleaseKernel(clear_integral_buff_kernel);
		clReleaseKernel(clear_output_buff_kernel);
		clReleaseMemObject(cl_result);
		freeComplexBuff(&cl_input);
		freeComplexBuff(&cl_output);
		clReleaseCommandQueue(commandQueue);
		clReleaseProgram(program);
		clReleaseContext(context);
		return EXIT_FAILURE;
	}

	
	//**************                 ***************//
	//*********     Enqueuing kernels     **********//
	//**************                 ***************//
	// second dimension is required for later uses
	size_t offset[2] = { 0, 0 };
	size_t global_work_size[2] = { NDRANGE_SIZE, 1 };
	size_t local_work_size[2]  = { 256, 1 }; // Test parameter
#ifdef USE_ECHO
	global_work_size[0] = RADIAL_SIZE;
	errNum = clEnqueueNDRangeKernel(commandQueue,
			__echo_kernel,
			1,
			offset,
			global_work_size,
			local_work_size,
			0,
			NULL,
			NULL);
	clEnqueueReadBuffer(commandQueue,
		cl_result,
		CL_TRUE,
		0,
		sizeof(cl_float) * OUTPUT_WIDTH * OUTPUT_HEIGHT,
		result,
		0,
		NULL,
		NULL);
	saveImage(result, "echo.jpeg", 2.f * RADIAL_SIZE * RADIAL_STEP / (OUTPUT_WIDTH - 1), AMPLITUDE_MODE);
#endif
	
	int num_iterations = NDRANGE_SIZE;
#ifdef DEBUG
	clock_t start = clock();
	printf("Number of iterations: %d\n\n", num_iterations);
#endif
	for (int iteration_id = 0; iteration_id < num_iterations, FALSE; iteration_id++) // TODO: remove FALSE
	{
		offset[0] = iteration_id;

		// Executing main computations
		cl_event compute_event;
		errNum = clEnqueueNDRangeKernel(commandQueue,
			compute_kernel,
			1,
			offset,
			global_work_size,
			local_work_size,
			0,
			NULL,
			&compute_event);
#ifdef DEBUG
		clWaitForEvents(1, &compute_event);
		cl_ulong integrate_start, integrate_end;
		clGetEventProfilingInfo(compute_event, CL_PROFILING_COMMAND_QUEUED, sizeof(cl_ulong), &integrate_start, NULL);
		clGetEventProfilingInfo(compute_event, CL_PROFILING_COMMAND_COMPLETE  , sizeof(cl_ulong), &integrate_end  , NULL);
		printf("%d. compute, sec: %f                   \r", iteration_id, (integrate_end - integrate_start) * 1e-9);
#endif
		clReleaseEvent(compute_event);
	}
#ifdef DEBUG
	clock_t end = clock();
	printf("\nTotal work-time, sec: %f\n", (double) (end - start) / CLOCKS_PER_SEC);
#endif

	global_work_size[0] = RADIAL_SIZE * PHI_SIZE;
	local_work_size[0] = 1;
	errNum = clEnqueueNDRangeKernel(commandQueue,
		clear_integral_buff_kernel,
		1,
		NULL,
		global_work_size,
		local_work_size,
		0,
		NULL,
		NULL);
	printf("Enqueue reading result...\n");
	clEnqueueReadBuffer(commandQueue,
		cl_integral_buff.Re,
		CL_TRUE,
		0,
		sizeof(cl_float) * OUTPUT_WIDTH * OUTPUT_HEIGHT,
		__test,
		0,
		NULL,
		NULL);
	normalize(__test, RADIAL_SIZE * PHI_SIZE); // normalize to 0xFF max
	extern void __savePolarAsImage(float*, char*);
	for(int indx = 0; indx < RADIAL_SIZE * PHI_SIZE; indx++)
		__test[indx] = 0;
	__savePolarAsImage(__test, "__test.jpeg");

	//**************                         ***************//
	//*********     Saving result as an image     **********//
	//**************                         ***************//
	unsigned char* char_image = (unsigned char*)malloc(3 * WORK_SIZE * WORK_SIZE);
	int i;

#ifdef DEBUG
	FILE *file;
	file = fopen("output_bytes.txt", "w");
#endif

	for(i = 0; i < WORK_SIZE * WORK_SIZE; i++)
	{
		BYTE colour[3];
#ifdef DEBUG
		i % WORK_SIZE == 0 ? fprintf(file, "\n") : i;
		fprintf(file, "%.2f\t", result[i]);
#endif
		getColour(__test[i], colour);
		char_image[3 * i + 0] = colour[2]; // Blue
		char_image[3 * i + 1] = colour[1]; // Green
		char_image[3 * i + 2] = colour[0]; // Red
	}

#ifdef DEBUG
	fclose(file);
#endif

	FIBITMAP* image = FreeImage_ConvertFromRawBits((BYTE*)char_image,
		OUTPUT_WIDTH,
		OUTPUT_HEIGHT,
		3 * OUTPUT_WIDTH,
		24,
		0xFF0000, 0x00FF00, 0x0000FF, FALSE);
	FreeImage_Save(FIF_JPEG, image, "images\\output.jpeg", 0);
	FreeImage_Unload(image);

	free(char_image);


	//Clean up
	free(result);
	free(__test);
	clReleaseKernel(getIntensity_kernel);
	clReleaseKernel(compute_kernel);
	clReleaseKernel(clear_integral_buff_kernel);
	clReleaseKernel(clear_output_buff_kernel);
	clReleaseMemObject(cl_result);
	freeComplexBuff(&cl_input);
	freeComplexBuff(&cl_output);
	freeComplexBuff(&cl_integral_buff);
	clReleaseCommandQueue(commandQueue);
	
	clReleaseProgram(program);
	clReleaseContext(context);
	return 0;
}

/*
 * Declarations of internal functions
 */
void __savePolarAsImage(float *buffer, char *filename);
void __savePhaseAsImage(float *polar_buffer, BYTE *bytes, float pix_size);
void __saveAmplitudeAsImage(float *ampl_buffer, BYTE *bytes, float pix_size);

/* getSymbolsNum: returns num of symbols in file or 0 in case of any mistake */
int getSymbolsNum(FILE *file)
{
	int num = 0;
	if (fseek(file, 0, SEEK_END) != 0)
	{
		printf("Cannot set cursor to the end of source.cl.\n");
		return 0;
	}
	if ((num = ftell(file)) == -1L)
	{
		printf("A file error has occurred.\n");
		return 0;
	}
	fseek(file, 0, SEEK_SET);

	return num;
}

/* errCheck: checks for error and in bad case printing error massage and return EXIT_FAILURE */
short errCheck(cl_int errNum, const char* error)
{
	if (errNum != CL_SUCCESS)
	{
		printf("ERROR: Fail in %s. Error code: %d", error, errNum);
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}

/* createContext: creates OpenCL context */
cl_context createContext(void)
{
	cl_int errNum;
	cl_uint num_platforms;
	cl_platform_id *platformIDs;

	// 1. Getting platforms
	errNum = clGetPlatformIDs(0, NULL, &num_platforms);
	if (errCheck(errNum, "clGetPlatformIDs(0, NULL, &num_platforms)") == EXIT_FAILURE)
		exit(EXIT_FAILURE);

	platformIDs = (cl_platform_id*)malloc(sizeof(cl_platform_id) * num_platforms);
	errNum = clGetPlatformIDs(num_platforms, platformIDs, NULL);
	if (errCheck(errNum, "clGetPlatformIDs") == EXIT_FAILURE)
	{
		free(platformIDs);
		exit(EXIT_FAILURE);
	}
	if (platformIDs == NULL) // Ensures that platformIDs is not NULL
	{
		free(platformIDs);
		exit(errCheck(CL_INVALID_PLATFORM, "clGetDeviceIDs, platformsIDs is empty."));
	}

	// 2. Getting devices for PLATFORM_INDEX platform
	cl_uint num_devices;
	cl_device_id *deviceIDs;
	errNum = clGetDeviceIDs(platformIDs[PLATFORM_INDEX], CL_DEVICE_TYPE_ALL, 0, NULL, &num_devices);
	if (errCheck(errNum, "clGetDeviceIDs(..., 0, NULL, &num_devices)") == EXIT_FAILURE)
	{
		free(platformIDs);
		exit(EXIT_FAILURE);
	}


	deviceIDs = (cl_device_id*)malloc(sizeof(cl_device_id) * num_devices);
	errNum = clGetDeviceIDs(platformIDs[PLATFORM_INDEX], CL_DEVICE_TYPE_ALL, num_devices, deviceIDs, NULL);
	if (errCheck(errNum, "clGetDeviceIDs(..., num_devices, deviceIDs, NULL)") == EXIT_FAILURE)
	{
		free(platformIDs);
		free(deviceIDs);
		exit(EXIT_FAILURE);
	}


	// 3. Creating context
	cl_context context;
	cl_context_properties properties[] =
	{
		CL_CONTEXT_PLATFORM, (cl_context_properties)platformIDs[PLATFORM_INDEX], 0
	};
	context = clCreateContext(properties, num_devices, deviceIDs, NULL, NULL, &errNum); // without callback
	if (errCheck(errNum, "clCreateContext(...)") == EXIT_FAILURE)
	{
		free(platformIDs);
		free(deviceIDs);
		clReleaseContext(context);
		exit(EXIT_FAILURE);
	}

	free(platformIDs);
	free(deviceIDs);
	// 4. Success! Return created context
	return context;
}

/* createProgram: creates and builds OpenCL program from source files */
cl_program createProgram(cl_context context)
{
	cl_int errNum;
	cl_uint num_devices;
	cl_device_id *devices;
	//cl_uint num_input_headers = 1;
	cl_program main_pg, params_pg;
	FILE *source_file, *params_file;
	char *source_str, *params_str;
	char *header_include_names[1] = { "params.h" };

	/* opening source files */
	if ((source_file = fopen("source.cl", "r")) == NULL)
	{
		printf("Cannot open source file(s).\n");
		clReleaseContext(context);
		exit(EXIT_FAILURE);
	}

	if ((params_file = fopen("params.h", "r")) == NULL)
	{
		printf("Cannot open source file(s).\n");
		fclose(source_file);
		clReleaseContext(context);
		exit(EXIT_FAILURE);
	}
	
	/* reading source files to strings */
	int source_len = getSymbolsNum(source_file);
	int params_len = getSymbolsNum(params_file);

	source_str = (char *)calloc((source_len + 1), sizeof(char));
	params_str = (char *)calloc((params_len + 1), sizeof(char));
	fread(source_str, sizeof(char), source_len, source_file);
	fread(params_str, sizeof(char), params_len, params_file);
	fclose(source_file), fclose(params_file);

	/* creating program objects */
	main_pg = clCreateProgramWithSource(context, 1, (const char **)&source_str, NULL, &errNum);
	if (errCheck(errNum, "clCreateProgramWithSource") == EXIT_FAILURE)
	{
		free(source_str), free(params_str);
		clReleaseProgram(main_pg);
		clReleaseContext(context);
		exit(EXIT_FAILURE);
	}
	params_pg = clCreateProgramWithSource(context, 1, (const char **)&params_str, NULL, &errNum);
	if (errCheck(errNum, "clCreateProgramWithSource") == EXIT_FAILURE)
	{
		free(source_str), free(params_str);
		clReleaseProgram(main_pg), clReleaseProgram(params_pg);
		clReleaseContext(context);
		exit(EXIT_FAILURE);
	}

	/* querying context for devices */
	errNum = clGetContextInfo(context, CL_CONTEXT_NUM_DEVICES, sizeof(cl_uint), &num_devices, NULL);
	if (errCheck(errNum, "clGetContextInfo(..., CL_CONTEXT_NUM_DEVICES, ...)") == EXIT_FAILURE)
	{
		free(source_str), free(params_str);
		clReleaseProgram(main_pg);
		clReleaseContext(context);
		exit(EXIT_FAILURE);
	}
	devices = (cl_device_id*)malloc(sizeof(cl_device_id) * num_devices);
	errNum = clGetContextInfo(context, CL_CONTEXT_DEVICES, sizeof(cl_device_id) * num_devices, devices, NULL);
	if (errCheck(errNum, "clGetContextInfo(..., CL_CONTEXT_DEVICES, ...)") == EXIT_FAILURE)
	{
		free(devices);
		free(source_str), free(params_str);
		clReleaseProgram(main_pg), clReleaseProgram(params_pg);
		clReleaseContext(context);
		exit(EXIT_FAILURE);
	}
	if (devices == NULL)
	{
		free(devices);
		free(source_str), free(params_str);
		clReleaseProgram(main_pg), clReleaseProgram(params_pg);
		clReleaseContext(context);
		exit(errCheck(CL_INVALID_DEVICE, "clGetContextInfo(..., CL_CONTEXT_DEVICES, ...), devices is empty."));
	}

	/* compile program objects */
	cl_program input_headers[1] = { params_pg };
	errNum = clCompileProgram(main_pg,
				1, &devices[DEVICE_INDEX],	// num_devices & device_list
				NULL,	// compile options
				1,
				input_headers,
				(const char**)header_include_names,
				NULL, NULL);	// pfn_notify & user_data

	/* building program */
	const char* build_options = NULL;
	errNum = clBuildProgram(main_pg, num_devices, devices, build_options, NULL, NULL); // without callback
	if (errNum != CL_SUCCESS)
	{
		// Determine the reason for the error
		char buildLog[15000];
		for (cl_uint i = 0; i < num_devices; i++)
		{
			errNum = clGetProgramBuildInfo(main_pg, devices[0], CL_PROGRAM_BUILD_LOG,
				sizeof(buildLog), buildLog, NULL);

			if (errCheck(errNum, "clGetProgramBuildInfo(..., CL_PROGRAM_BUILD_LOG, ...)") == EXIT_FAILURE)
			{
				free(devices);
				free(source_str), free(params_str);
				clReleaseProgram(main_pg), clReleaseProgram(params_pg);
				clReleaseContext(context);
				exit(EXIT_FAILURE);
			}
			printf("ERROR: Building error on device #%d. Build log:\n", i);
			printf("%s\n", buildLog);
		}
		free(devices);
		free(source_str), free(params_str);
		clReleaseProgram(main_pg), clReleaseProgram(params_pg);
		clReleaseContext(context);
		exit(EXIT_FAILURE);
	}

	free(devices);
	free(source_str), free(params_str);
	return main_pg;
}

/* createCommandQueue: creates OpenCL queue for given context, program and device index */
cl_command_queue createCommandQueue(cl_context context, cl_program program)
{
	cl_int errNum;
	cl_uint num_devices;
	cl_device_id *devices;
	cl_command_queue queue;

	// Querying context for devices
	errNum = clGetContextInfo(context, CL_CONTEXT_NUM_DEVICES, sizeof(cl_uint), &num_devices, NULL);
	if (errCheck(errNum, "clGetContextInfo(..., CL_CONTEXT_NUM_DEVICES, ...)") == EXIT_FAILURE)
	{
		clReleaseProgram(program);
		clReleaseContext(context);
		exit(EXIT_FAILURE);
	}
	devices = (cl_device_id*)malloc(sizeof(cl_device_id) * num_devices);
	errNum = clGetContextInfo(context, CL_CONTEXT_DEVICES, sizeof(cl_device_id) * num_devices, devices, NULL);
	if (errCheck(errNum, "clGetContextInfo(..., CL_CONTEXT_DEVICES, ...)") == EXIT_FAILURE)
	{
		free(devices);
		clReleaseProgram(program);
		clReleaseContext(context);
		exit(EXIT_FAILURE);
	}
	if (devices == NULL)
	{
		free(devices);
		clReleaseProgram(program);
		clReleaseContext(context);
		exit(errCheck(CL_INVALID_DEVICE, "clGetContextInfo(..., CL_CONTEXT_DEVICES, ...), devices is empty."));
	}

	// Creating command queue
	cl_command_queue_properties properties[] =
	{
		CL_QUEUE_PROPERTIES, CL_QUEUE_PROFILING_ENABLE, 0
	};
	queue = clCreateCommandQueueWithProperties(context, devices[DEVICE_INDEX], properties, &errNum);
	if (errCheck(errNum, "clCreateQueueWithProperties") == EXIT_FAILURE)
	{
		free(devices);
		clReleaseProgram(program);
		clReleaseContext(context);
		exit(EXIT_FAILURE);
	}

	free(devices);
	return queue;
}

/* normalize: normalizes buffer to range [0,255] */
float *normalize(float* buffer, int size)
{
	int x_min, y_min, x_max, y_max;
	float max_element = buffer[0];
	float min_element = buffer[0];
	int i;

	x_min = y_min = x_max = y_max = 0;

#ifdef DEBUG
	FILE* file;
	file = fopen("unnormalized_buffer.txt", "w");
#endif

	for (i = 0; i < size; i++)
	{
#ifdef DEBUG
		fprintf(file, "%.2f\n", buffer[i]);
#endif
		min_element = min_element > buffer[i] ? buffer[i] : min_element;
		max_element = max_element < buffer[i] ? buffer[i] : max_element;
	}
#ifdef DEBUG
	fclose(file);
#endif
	
	for (i = 0; i < size; i++)
	{
		buffer[i] -= min_element;						 		//buffer ranges from 0 to (max_element - min_element)
		if (max_element == min_element)
			buffer[i] *= 0x80;
		else
			buffer[i] *= 0xFF / (max_element - min_element);	//buffer ranges from 0 to 255
	}

	return buffer;
}

/* fillPolarInput: fills input_ampl and input_phase */
void fillPolarInput(float complex *field)
{
	float *intensity = (float*)malloc(RADIAL_SIZE * PHI_SIZE * sizeof(float));

	for(unsigned int r_i = 0; r_i < RADIAL_SIZE; r_i++)
	{
		for(unsigned int phi_i = 0; phi_i < PHI_SIZE; phi_i++)
		{
			field[phi_i + r_i * PHI_SIZE] = 255.f * cexpf(-cpowf(r_i * RADIAL_STEP / BEAM_RADIUS, 2));
			intensity[phi_i + r_i * PHI_SIZE] = powf(crealf(field[phi_i + r_i * PHI_SIZE]), 2) + powf(cimagf(field[phi_i + r_i * PHI_SIZE]), 2);
		}
	}

	// TODO: change signature of saveImage
	/*
	 * Calculating pixel size:
	 * ((OUTPUT_WIDTH - 1) / 2) * pix_size =  RADIAL_STEP * RADIAL_SIZE,
	 *  
	 * so pix_size = RADIAL_STEP * RADIAL_SIZE * 2 / (OUTPUT_WIDTH - 1)
	 */
	normalize(intensity, RADIAL_SIZE * PHI_SIZE);
	saveImage(intensity, "input_intensity.bmp", 2.f * RADIAL_STEP * (float)RADIAL_SIZE / (float)(OUTPUT_WIDTH - 1), POLAR_MODE);
	free(intensity);
	return;
}

/* saveImage: saves buffer of size width x height pixels as image; returns error code*/
int saveImage(float *buffer, char *filename, float pix_size, save_mode_t mode)
{
	char path[30];
	sprintf(path, "images\\%s", filename);
	BYTE *bytes = (BYTE*)malloc(3 * OUTPUT_WIDTH * OUTPUT_HEIGHT); // 3 - three colours: RGB

	switch(mode)
	{
		case POLAR_MODE:
			__savePolarAsImage(buffer, filename);
			break;
		default:
			printf("Error while saving image. Incorrect mode.");
			return EXIT_FAILURE;
	}

	FIBITMAP* image = FreeImage_ConvertFromRawBits(bytes,
		OUTPUT_WIDTH,
		OUTPUT_HEIGHT,
		3 * OUTPUT_WIDTH,
		24,
		0xFF0000, 0x00FF00, 0x0000FF, FALSE);
	FreeImage_Save(FIF_JPEG, image, path, 0);
	FreeImage_Unload(image);

	free(bytes);
	return EXIT_SUCCESS;
}

// TODO: change algorithm so that every pixel is filled with color
void __savePolarAsImage(float *buffer, char *filename)
{
	RGBQUAD color = { 0, 0, 0 };
	RGBQUAD blur_color[9];
	FIBITMAP *bitmap = FreeImage_Allocate(2 * RADIAL_SIZE - 1,
	                                      2 * RADIAL_SIZE - 1,
										  24,
	                                      FI_RGBA_RED_MASK,
										  FI_RGBA_GREEN_MASK,
										  FI_RGBA_BLUE_MASK);
	for(unsigned int r_i = 0; r_i < RADIAL_SIZE; r_i++)
	{
		for(unsigned int phi_i = 0; phi_i < PHI_SIZE; phi_i++)
		{
			color.rgbBlue = buffer[phi_i + r_i * PHI_SIZE];
			FreeImage_SetPixelColor(bitmap,
			                        (int)(r_i * cos(2 * PI * phi_i / (float)PHI_SIZE)) + RADIAL_SIZE - 1,
									(int)(r_i * sin(2 * PI * phi_i / (float)PHI_SIZE)) + RADIAL_SIZE - 1,
									&color);
									// Checked: x,y range from 0 to (2R - 2).
		}
	}

	// Bluring
	for(unsigned int x = 1; x < FreeImage_GetWidth(bitmap) - 1; x++)
	{
		for(unsigned int y = 1; y < FreeImage_GetHeight(bitmap) - 1; y++)
		{
			FreeImage_GetPixelColor(bitmap, x - 1, y - 1, &blur_color[0]);
			FreeImage_GetPixelColor(bitmap, x    , y - 1, &blur_color[1]);
			FreeImage_GetPixelColor(bitmap, x + 1, y    , &blur_color[2]);
			FreeImage_GetPixelColor(bitmap, x - 1, y    , &blur_color[3]);
			FreeImage_GetPixelColor(bitmap, x    , y    , &blur_color[4]);
			FreeImage_GetPixelColor(bitmap, x + 1, y    , &blur_color[5]);
			FreeImage_GetPixelColor(bitmap, x - 1, y + 1, &blur_color[6]);
			FreeImage_GetPixelColor(bitmap, x    , y + 1, &blur_color[7]);
			FreeImage_GetPixelColor(bitmap, x + 1, y + 1, &blur_color[8]);
			color.rgbBlue = (blur_color[0].rgbBlue + blur_color[1].rgbBlue + blur_color[2].rgbBlue +
							 blur_color[3].rgbBlue + blur_color[4].rgbBlue + blur_color[5].rgbBlue +
							 blur_color[6].rgbBlue + blur_color[7].rgbBlue + blur_color[8].rgbBlue) / 9.f;
			color.rgbGreen = (blur_color[0].rgbGreen + blur_color[1].rgbGreen + blur_color[2].rgbGreen +
							  blur_color[3].rgbGreen + blur_color[4].rgbGreen + blur_color[5].rgbGreen +
							  blur_color[6].rgbGreen + blur_color[7].rgbGreen + blur_color[8].rgbGreen) / 9.f;
			color.rgbRed = (blur_color[0].rgbRed + blur_color[1].rgbRed + blur_color[2].rgbRed +
							blur_color[3].rgbRed + blur_color[4].rgbRed + blur_color[5].rgbRed +
							blur_color[6].rgbRed + blur_color[7].rgbRed + blur_color[8].rgbRed) / 9.f;
			FreeImage_SetPixelColor(bitmap, x, y, &color);
		}
	}

	FreeImage_Save(FIF_BMP, bitmap, filename, 0);
	FreeImage_Unload(bitmap);
}

// TODO: make error return
void __savePhaseAsImage(float *polar_buffer, BYTE *bytes, float pix_size)
{
	int i, j;	// (i,j) - cartesian indexes
	float x, y;	// (x,y) - cartesian coordinates
	int p, q;		// (p,q) - polar indexes
	float rho, phi;	// (rho, phi) - polar coordinates

	FILE *file;
	file = fopen("phase.txt", "w");
	for(i = 0; i < OUTPUT_WIDTH; i++)
	{
		for(j = 0; j < OUTPUT_HEIGHT; j++)
		{
			x = (i - (OUTPUT_WIDTH - 1) / 2) * pix_size;
			y = (j - (OUTPUT_HEIGHT - 1) / 2) * pix_size;
			rho = sqrtf(x * x + y * y);
			phi = PI / 2 - atanf(x / y) + (y < 0 ? PI : 0);
			phi != phi ? phi = 0 : phi;
			p = (int)(rho / RADIAL_STEP);
			q = (int)(phi / PHI_STEP);
			if (q + p * PHI_SIZE >= RADIAL_SIZE * PHI_SIZE)
				continue;
			BYTE colour[3]; //3 <=> B, G, R
			getColour(polar_buffer[q + p * PHI_SIZE], colour);
			fprintf(file, "%d\t", colour[0] + colour[1] + colour[2]);
			bytes[3 * (i + j * OUTPUT_WIDTH) + 0] = colour[2]; // Blue
			bytes[3 * (i + j * OUTPUT_WIDTH) + 1] = colour[1]; // Green
			bytes[3 * (i + j * OUTPUT_WIDTH) + 2] = colour[0]; // Red
		}
		fprintf(file, "\n");
	}
	free(file);
	return;
}

// TODO: make error return
void __saveAmplitudeAsImage(float *ampl_buffer, BYTE *bytes, float pix_size)
{
	int i, j;	// (i,j) - cartesian indexes
	float x, y;	// (x,y) - cartesian coordinates
	int p;	// (p) - ampl_buffer index
	float rho;	// (rho, phi) - polar coordinates

#ifdef DEBUG
	FILE *file;
	file = fopen("amplitude.txt", "w");
#endif
	for(i = 0; i < OUTPUT_WIDTH; i++)
	{
		for(j = 0; j < OUTPUT_HEIGHT; j++)
		{
			x = (i - (OUTPUT_WIDTH - 1) / 2) * pix_size;
			y = (j - (OUTPUT_HEIGHT - 1) / 2) * pix_size;
			rho = sqrtf(x * x + y * y);
			p = (int)(rho / RADIAL_STEP);
			if (p >= RADIAL_SIZE)
				continue;
			BYTE colour[3]; //3 <=> B, G, R
			getColour(ampl_buffer[p], colour);
#ifdef DEBUG
			fprintf(file, "%d\t", colour[0] + colour[1] + colour[2]);
#endif
			bytes[3 * (i + j * OUTPUT_WIDTH) + 0] = colour[2]; // Blue
			bytes[3 * (i + j * OUTPUT_WIDTH) + 1] = colour[1]; // Green
			bytes[3 * (i + j * OUTPUT_WIDTH) + 2] = colour[0]; // Red
		}
#ifdef DEBUG
		fprintf(file, "\n");
#endif
	}
#ifdef DEBUG
	free(file);
#endif
	return;
}

// TODO: remove obsolete __saveInputImage
/*__saveInputImage: saves real_input to file*/
void __saveInputImage(float* real_input, char *filename, save_mode_t mode)
{
	char __filename[30];
	sprintf(__filename, "images\\%s", filename);
	BYTE* raw_image = (BYTE*)malloc(3 * INPUT_WIDTH * INPUT_HEIGHT);
	int i;
	for(i = 0; i < INPUT_WIDTH * INPUT_HEIGHT; i++)
	{
		BYTE colour[3];
		if (mode == INPUT_MODE)
			getColour(real_input[i] + 128, colour);
		else
			getColour(real_input[i], colour);
		raw_image[3 * i + 0] = colour[2]; // Blue
		raw_image[3 * i + 1] = colour[1]; // Green
		raw_image[3 * i + 2] = colour[0]; // Red
	}

	FIBITMAP* image = FreeImage_ConvertFromRawBits(raw_image,
		INPUT_WIDTH,
		INPUT_HEIGHT,
		3 * INPUT_WIDTH,
		24,
		FI_RGBA_RED_MASK, FI_RGBA_GREEN_MASK, FI_RGBA_BLUE_MASK, FALSE);
	FreeImage_Save(FIF_JPEG, image, __filename, 0);
	FreeImage_Unload(image);

	free(raw_image);
	return;
}

/* getColour: convert gray value ranged from 0 to 255 to jet colour map */
void getColour(float gray_value, BYTE* output_colour)
{
	/* Colour map sequence:
		#00007F: dark blue
		#0000FF: blue
		#007FFF: azure
		#00FFFF: cyan
		#7FFF7F: light green
		#FFFF00: yellow
		#FF7F00: orange
		#FF0000: red
		#7F0000: dark red
	*/

	// Range check
	if(gray_value < 0x00)
	{
		printf("getColour: gray_value < 0x00\n");
		gray_value = 0;
	}
	if(gray_value > 0xFF)
	{
		printf("getColour: gray_value > 0xFF\n");
		gray_value = 0xFF;
	}

	double colour[3] = { 0, 0, 0 }; // RGB

	if (gray_value < 0.125 * 0xFF) {
		colour[2] = 0x7F; // starting with DARK BLUE
		colour[2] += (0xFF - 0x7F) * gray_value / (0.125 * 0xFF);
	} else if (gray_value < 0.250 * 0xFF) {
		colour[2] = 0xFF; // starting with BLUE
		colour[1] += 0x7F * (gray_value - 0.125 * 0xFF) / (0.125 * 0xFF);
	} else if (gray_value < 0.375 * 0xFF) {
		colour[2] = 0xFF; //
		colour[1] = 0x7F; // starting with AZURE
		colour[1] += (0xFF - 0x7F) * (gray_value - 0.250 * 0xFF) / (0.125 * 0xFF);
	} else if (gray_value < 0.500 * 0xFF) {
		colour[2] = 0xFF; //
		colour[1] = 0xFF; // starting with CYAN
		colour[2] -= (0xFF - 0x7F) * (gray_value - 0.375 * 0xFF) / (0.125 * 0xFF);
		colour[0] += 0x7F * (gray_value - 0.375 * 0xFF) / (0.125 * 0xFF);
	} else if (gray_value < 0.625 * 0xFF) {
		colour[2] = 0x7F; //
		colour[1] = 0xFF; //
		colour[0] = 0x7F; // starting with light green
		colour[0] += (0xFF - 0x7F) * (gray_value - 0.500 * 0xFF) / (0.125 * 0xFF);
		colour[2] -= 0x7F * (gray_value - 0.500 * 0xFF) / (0.125 * 0xFF);
	} else if (gray_value < 0.750 * 0xFF) {
		colour[2] = 0x00; //
		colour[1] = 0xFF; //
		colour[0] = 0xFF; // starting with YELLOW
		colour[1] -= (0xFF - 0x7F) * (gray_value - 0.625 * 0xFF) / (0.125 * 0xFF);
	} else if (gray_value < 0.875 * 0xFF) {
		colour[2] = 0x00; //
		colour[1] = 0x7F; //
		colour[0] = 0xFF; // starting with ORANGE
		colour[1] -= 0x7F * (gray_value - 0.750 * 0xFF) / (0.125 * 0xFF);
	} else {
		colour[2] = 0x00; //
		colour[1] = 0x00; //
		colour[0] = 0xFF; // starting with RED
		colour[0] -= 0x7F * (gray_value - 0.875 * 0xFF) / (0.125 * 0xFF);
	}

	output_colour[0] = (BYTE)colour[0];
	output_colour[1] = (BYTE)colour[1];
	output_colour[2] = (BYTE)colour[2];

	return;
}

// TODO: check whether it works. I doubt __buffer can be rewritten before its content is loaded into GPU
static float __buffer[RADIAL_SIZE * PHI_SIZE];

float *GetRealArray(float complex *buff)
{
	uint32_t i;
	for(i = 0; i < RADIAL_SIZE * PHI_SIZE; i++)
		__buffer[i] = crealf(buff[i]);
	return __buffer;
}

float *GetImagArray(float complex *buff)
{
	uint32_t i;
	for(i = 0; i < RADIAL_SIZE * PHI_SIZE; i++)
		__buffer[i] = cimagf(buff[i]);
	return __buffer;
}
